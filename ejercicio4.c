#include <stdio.h>

void main() {
    int fil, col, size;
    printf("Ingrese el tamaño de la matriz nxn\n");
    scanf("%d", &size);
    int a[size][size];
    int b[size][size];
    if (size>1) {

    

    printf("Ingrese los datos a la matriz \n");
    for (fil = 0; fil < size; fil++) {
        for (col = 0; col < size; col++) {
            printf("Numero (%d,%d): ", fil+1, col+1);
            scanf("%d", &a[fil][col]);
        }
    }
    printf("\nMATRIZ A");
    for (fil = 0; fil < size; fil++) {
        printf("\n");
        for (col = 0; col < size; col++) {
            printf("%d  ", a[fil][col]);
        }
    }

    col = 1;
    int suma = 0;
    printf("\n\nLa diagonal con pendiente positiva es:\n");
    for (fil = 0; fil < size; fil++) {
        printf("%d\t", a[fil][size - col]);
        suma = suma + a[fil][size - col];
        col = col + 1;
    }
    printf("\nY su suma es: %d", suma);


    col = 0;
    suma = 0;
    printf("\n\nLa diagonal con pendiente negativa es:\n");
    for (fil = 0; fil < size; fil++) {
        printf("%d\t", a[fil][col]);
        suma = suma + a[fil][col];
        col = col + 1;
    }
    printf("\nY su suma es: %d\n", suma);


    col = 0;
    suma = 0;
    for (fil = 0; fil < size; fil++) {
        printf("\nLos números de la fila %d son:\n", fil + 1);
        for (col = 0; col < size; col++) {
            printf("%d\t", a[fil][col]);
            suma = suma + a[fil][col];
        }
        printf("\nY la suma es: %d\n", suma);

    }


    col = 0;
    suma = 0;
    for (col = 0; col < size; col++) {
        printf("\nLos números de la columna %d son:\n", col + 1);
        for (fil = 0; fil < size; fil++) {
            printf("%d\t", a[fil][col]);
            suma = suma + a[fil][col];
        }
        printf("\nY la suma es: %d\n", suma);

    }
    }else{
        printf("Tamaño de la matriz invalido");
    }

}