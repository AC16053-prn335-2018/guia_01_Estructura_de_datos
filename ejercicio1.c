#include <stdio.h>
#include <stdlib.h>

void main() {
    int fil = 0, col = 0, aux = 0, size;
    int i = 0, j = 0, h = 0;
    printf("Ingrese el tamaño de las matrices nxn: \n");
    scanf("%d", &size);
    if (size>1) {

    

    int matriz1[size][size];
    int matriz2[size][size];
    int matriz_resultante[size][size];

    //LLENANDO MATRIZ 1
    printf("Ingrese la matriz 1:\n");
    for (fil = 0; fil < size; fil++) {
        for (col = 0; col < size; col++) {
            printf("Posicion (%d,%d): ", fil + 1, col + 1);
            scanf("%d", &matriz1[fil][col]);

        }
    }

    //LLENANDO MATRIZ 2
    printf("Ingrese la matriz 2:\n");
    for (fil = 0; fil < size; fil++) {
        for (col = 0; col < size; col++) {
            printf("Posicion (%d,%d): ", fil + 1, col + 1);
            scanf("%d", &matriz2[fil][col]);
        }
    }

    //MOSTRAR MATRIZ 1
    printf("\n Matriz 1:\n");
    for (int fil = 0; fil < size; fil++) {
        printf("[ ");
        for (int col = 0; col < size; col++) {
            printf("%d ", matriz1[fil][col]);
        }
        printf("]");
        printf("\n");
    }
    //MOSTRAR MATRIZ 2 
    printf("\n Matriz 2:\n");
    for (int fil = 0; fil < size; fil++) {
        printf("[ ");
        for (int col = 0; col < size; col++) {
            printf("%d ", matriz2[fil][col]);
        }
        printf("]");
        printf("\n");
    }

    //CREARA LA MATRIZ RESULTANTE
    printf("La matriz resultante es: \n");

    for (int i = 0; i < size; i++) {
        printf("[ ");

        for (int j = 0; j < size; j++) {
            matriz_resultante[i][j] = 0;
            for (int h = 0; h < size; h++) {
                matriz_resultante[i][j] = matriz_resultante[i][j]+(matriz1[i][h] * matriz2[h][j]);
            }
            printf("%d ", matriz_resultante[i][j]);
        }
        printf("]");
        printf("\n");
    }
    }else{
         printf("Tamaño de la matriz invalido");
    }

}